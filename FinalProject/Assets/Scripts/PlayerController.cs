﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    
	private Rigidbody rb;
    void Start()
        {
            rb = GetComponent<Rigidbody>();
        }
    public float speed;
    
    	private void FixedUpdate()
        {
        if (Input.GetKeyDown("space"))
            this.rb.AddForce(Vector3.up * 20000.0f);
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");

            Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
            rb.velocity = movement * speed;
        }
    }