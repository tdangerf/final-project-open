﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    public int starthealth;
    public int curhealth;
    bool isDead;
    Animator anim;
    bool iswound;
	// Use this for initialization
	void Start () {
        curhealth = starthealth;
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
       
	}
    public void damage(int amount, Vector3 hitPoint)
    {
        if (isDead)
            return;
        iswound = true;
        curhealth -= amount;
        if (curhealth <= 0)
        { Death(); }
    }
    public void wounded()
    {
        if (iswound)
        {
            anim.Play("troll@wound1");
        }
    }

    void Death()
    {
        isDead = true;
            // make troll be ground or use anim if available
        Destroy(gameObject, 2f);
    }
}
