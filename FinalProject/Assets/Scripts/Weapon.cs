﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {
	public int damage = 5;

	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.tag == "Enemy") {
			other.gameObject.GetComponent<Health> ().curhealth -= damage;
			DestroyObject (other.gameObject);
		}
	}
}
