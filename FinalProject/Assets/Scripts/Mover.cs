﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    public float movespeed = 3f;
    public float rotspeed = 100f;
    private bool iswander = false;
    private bool isrotleft = false;
    private bool isrotright = false;
    private bool iswalk = false;
    private Rigidbody rb;
    void Start()
    {
       
    }
    void Update()
    {
        if (iswander == false)
        {
            StartCoroutine(Wander());
        }
        if (isrotright == true)
        {
            transform.Rotate(transform.up * Time.deltaTime * rotspeed);
        }
        if (isrotleft == true)
        {
            transform.Rotate(transform.up * Time.deltaTime * -rotspeed);
        }
        if (iswalk == true)
        {
            transform.position += transform.forward * movespeed * Time.deltaTime;
        }

    }
    IEnumerator Wander()
    {
        int rottime = Random.Range(1, 3);
        int rotwait = Random.Range(1, 4);
        int rotlr = Random.Range(1, 2);
        int walkwait = Random.Range(1, 4);
        int walktime = Random.Range(1, 5);

        iswander = true;
        yield return new WaitForSeconds(walkwait);
        iswalk = true;
        yield return new WaitForSeconds(walktime);
        iswalk = false;
        yield return new WaitForSeconds(rotwait);
        if (rotlr == 1)
        {
            isrotright = true;
            yield return new WaitForSeconds(rottime);
            isrotright = false;
        }
        if (rotlr == 2)
        {
            isrotleft = true;
            yield return new WaitForSeconds(rottime);
            isrotleft = false;
        }
        iswander = false;
    }

}
